FROM amazon/aws-cli:2.3.3

ARG ROSA_VERSION=v1.1.5

RUN curl -o /usr/local/bin/rosa -L "https://github.com/openshift/rosa/releases/download/$ROSA_VERSION/rosa-linux-amd64" \
    && chmod o+x /usr/local/bin/rosa

RUN  yum install tar gzip -y \
    && rosa download oc \
    && tar xfz openshift-client-linux.tar.gz \
    && mv oc /usr/local/bin \
    && mv kubectl /usr/local/bin \
    && rm openshift-client-linux.tar.gz

ENTRYPOINT [ "/bin/bash" ]