# rosa-cli

Container image using homebrew/linuxbrew providing RedHat OpenShift on AWS CLI (rosa-cli)

### What it's all about

Inside a GitLab CI based project we needed RedHat OpenShift on AWS CLI aka [rosa-cli](https://docs.openshift.com/rosa/rosa_cli/rosa-get-started-cli.html) available.

As there is no public image available atm we decided to create one ourselves. Since the homebrew based images were way to big (2,5gig), we needed another alternative. And as the [homebrew rosa-cli formulae](https://formulae.brew.sh/formula/rosa-cli) has a dependency to aws-cli,  we switched to the official AWS CLI container image:

* https://hub.docker.com/r/amazon/aws-cli
* with the Dockerfile https://github.com/aws/aws-cli/blob/v2/docker/Dockerfile (which tells us the image is based on amazonlinux:2)

> why not use `we provided an official GitLab AWS cloud-deploy Docker image` mentioned in this blog: https://about.gitlab.com/blog/2020/12/15/deploy-aws/ ?

Because the mentioned [Dockerfile](https://gitlab.com/gitlab-org/cloud-deploy/-/blob/master/aws/cloud_deploy/Dockerfile) was developed at hacktoberfest, but can't compete against the official amazon/aws-cli image at first look 

### Our GitLab setup: No privileged Docker socket or docker cli

As [a state of the art GitLab setup](https://blog.codecentric.de/en/2021/10/gitlab-ci-paketo-buildpacks/) comprises of non-privileged Kubernetes runners without a mounted or available Docker socket or cli.

So we needed to use Kaniko https://docs.gitlab.com/ee/ci/docker/using_kaniko.html

This means we __must__ write a [Dockerfile](Dockerfile) again.


### Download rosa CLI

As [the rosa Docs bring us to a RedHat Account wall](https://access.redhat.com/products/red-hat-openshift-service-aws), we needed another way to download rosa-cli.

Luckily rosa is developed on GitHub at https://github.com/openshift/rosa/ and there are releases you can simply download: https://github.com/openshift/rosa/releases

As the GitHub Release page download links redirect to the actual files, we need to [use `curl` with the `-L` option to get the file without a ugly url](https://stackoverflow.com/a/46060099/4964553).

So start with this [Dockerfile](Dockerfile):

```shell
FROM amazon/aws-cli:2.3.3

ARG ROSA_VERSION=v1.1.5

RUN curl -o /usr/local/bin/rosa -L "https://github.com/openshift/rosa/releases/download/$ROSA_VERSION/rosa-linux-amd64" \
    && chmod o+x /usr/local/bin/rosa

ENTRYPOINT [ "/usr/local/bin/rosa" ]
```

We also download rosa binary to `/usr/local/bin/` to be available in the `PATH` out of the box. Now don't forget to `chmod o+x /usr/local/bin/rosa` to make the file executable.

And we finally override the `ENTRYPOINT` to have rosa accessible per default. Try it with:

```shell
docker run registry.gitlab.com/jonashackt/rosa-cli version
```

### Pinning rosa version as Docker image tag

Lets also define the rosa version inside our [.gitlab-ci.yml](.gitlab-ci.yml) and use a `--build-arg` for Kaniko to pipe it to our Docker build process: 

```yaml
variables:
  ROSA_VERSION: v1.1.5

...

/kaniko/executor
      --context "${CI_PROJECT_DIR}"
      --dockerfile "${CI_PROJECT_DIR}/Dockerfile"
      --destination "${CI_REGISTRY_IMAGE}:${ROSA_VERSION}"
      --build-arg ROSA_VERSION=${ROSA_VERSION}
```

Now you can run the image with

```
docker run registry.gitlab.com/jonashackt/rosa-cli:v1.1.5 version
```

Or use it inside another project's [.gitlab-ci.yml](.gitlab-ci.yml).


### Add oc and kubectl

[To interact with our rosa cluster](https://github.com/jonashackt/openshift-aws) we also need to have `oc` and `kubectl` available.

So therefore use `rosa download oc` like this (and install tar gzip before and unzip afterwards):

```dockerfile
...
RUN  yum install tar gzip -y \
    && rosa download oc \
    && tar xfz openshift-client-linux.tar.gz \
    && mv oc /usr/local/bin \
    && mv kubectl /usr/local/bin \
    && rm openshift-client-linux.tar.gz
    
ENTRYPOINT [ "/bin/bash" ]
```

Don't forget to set the `ENTRYPOINT` to `/bin/bash` again to not default to `rosa`.

Now we also have `oc` and `kubectl` available inside our image! :)